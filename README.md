# sardi-icons

Sardi is an icon collection for any linux distro with 6 different circular icons and 10 different kind of folders.

https://sourceforge.net/projects/sardi/files/

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/icons-and-themes/sardi-icons.git
```
